README.md

Jonathan Takeshita, Anjana Rajagopal, Meredith Ostermann, Tim Richards

EECS498

HoloChart


DEPLOYMENT INSTRUCTIONS:
To deploy:
1. Install all required software described by https://developer.microsoft.com/en-us/windows/mixed-reality/install_the_tools . (The emulator and vuforia are not strictly necessary.)
2. Open the project directory in Unity.
3. Run a Unity Build: go to File -> Build Settings, set the Target device to HoloLens, and the Build Type to XAML. Leave the SDK as latest installed, and the Build and Run on option as Local Machine. Make sure the Platform is the Universal Windows Platform. Click Build, and select an empty directory from the file picker.
4. Run a Visual Studio Build: In the previously empty folder, there should now be a .sln file. Open this with Visual Studio. At the top of the screen, set the mode to Release or Debug, the architecture to x86, and the deployment platform (next to the green arror) to Device. Connect your HoloLens to your computer via USB. Then, go to Build -> Deploy HoloChart to build the final program and deploy it to the HoloLens.
5. Once the previous step is finished, start using the HoloLens. Use the "bloom" gesture to bring up the menu, click the '+' on the right to show more apps, and select HoloChart from the list.

This is a tutorial video on how to use our application: https://youtu.be/5fObrJ_MiOo