﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

//TODO move this file to outside the HTK

public class TutorialVideoManager : MonoBehaviour {

    public VideoPlayer vp;

    public void Start()
    {

    }

    public void PlayMovie()
    {
        if(vp == null)
        {
            throw new System.Exception("Video player unassigned!");
        }

        gameObject.SetActive(true);
        vp.Play();
        gameObject.SetActive(false);
    }

    public bool isPlaying()
    {
        return vp.isPlaying;
    }

	
	// Update is called once per frame
	void Update () {
        /*
        if (!isStarted)
        {
            return;
        }
        if (movie.isPlaying)
        {
            return;
        }
        else
        {
            movie.Stop();
        }
        */
	}
}
