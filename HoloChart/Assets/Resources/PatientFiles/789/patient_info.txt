Name:Debra Hendrickson

Medical Record Number:789

#height is passed as a 3-digit number for imperial

#fields requiring units have the units in parenthesis after the value

Age:21

Sex:Female

Weight:120

Allergies:Peanuts,Chlorine,Sunlight,Bees,Fish,Apples,Dust,Pollen,Cats,Dogs,Poison ivy,Virginia creeper,Nectarines,Kiwi,Salt water,Coconuts,Lye

Height:160

Vitals Dates:2010,3,4;2011,4,5

Heart Rate:65,66

Blood Pressure:69,99;70,100

Pulse Oximetry:89,74

Respiratory Rate:40,41

#year, month, day

Birthday:1996,3,16

Symptoms:Aching back,sharp headache,bedridden

Lab Reports:broken_leg_lab_report

Medications:Excedrin Extra Strength Pain Reliever,Benadryl LiquiGels,Tylenol,Warfarin,Amoxicillin,Asprin,Menthol,Sudafed,Lipitor,Viagra,Bacta,Daily multivitamin,Silver,Eucerin,Hydrocortisone