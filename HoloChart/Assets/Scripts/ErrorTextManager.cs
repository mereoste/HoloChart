﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorTextManager : MonoBehaviour {

    public Text errorText;

	// Use this for initialization
	void Start () {
        errorText.CrossFadeAlpha(0.0f, 0.0f, false);
        errorText.color = Color.red;
	}

    public void ErrorAlert(string newErrorText)
    {
        errorText.text = newErrorText;
        errorText.CrossFadeAlpha(1.0f, 0.0f, false);
        Fade();
    }

    private void Fade()
    {
        errorText.CrossFadeAlpha(0.0f, 3.0f, false);
    }
    public void setText(string text)
    {
        errorText.text = text;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
