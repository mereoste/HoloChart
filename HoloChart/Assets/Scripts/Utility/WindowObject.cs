﻿using UnityEngine;

public class WindowObject : MonoBehaviour {

    public WindowManager windowManager;
    public ToggleTagalong toggleTagalong;

    public void CloseWindow()
    {
        windowManager.ButtonClick();
    }
}
