﻿using HoloToolkit.Unity;
using UnityEngine;

public class DisableTagalongOnStart : MonoBehaviour {

    public SimpleTagalong simpleTagalong;
    
	void Start ()
    {
        simpleTagalong.enabled = false;
	}
}
