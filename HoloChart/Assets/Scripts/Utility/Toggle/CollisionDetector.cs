﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * TO USE:
 * Assign each ref. in ToggleVisibility and ToggleTagalong for every object that needs coll. detection
 * Add/remove RTs when they are made visible/invisible
 */

public class CollisionDetector : MonoBehaviour
{
    //ErrorTextManager errortextmanager;
    private const int BOTTOMLEFT = 0;
    private const int TOPRIGHT = 2;
    //Amount to move canvases back/forwards by - may need tweaking
    private const int MOVEDIST = 1;
    //Directions
    private const bool FORWARDS = true;
    private const bool BACKWARDS = false;

    //Camera - set in Unity
    public Camera cam;

    //Bool is true iff canvas is pushed back
    //Maps RectTransform to bool
    private Hashtable rectangles;
    //RectTransform storing the currently moving RectTransform, null if none is moving
    private RectTransform currMovingRT;

    public ErrorTextManager errorTextManager;

    //Use this function to add a RectTransform to the list of moving rectangles
    //Returns true if permissible and no errors
    public bool addToDetector(RectTransform rt, bool isInMoveState)
    {
        //rt is invalid, or has already been added
        if(rt == null || rectangles.ContainsKey(rt))
        {
            return false;
        }
        if(isInMoveState && inMotion())
        {
            //Debug.Log("A canvas is already moving");
            return false;
        }
        //Set rectangle in hash table
        //Assign new moving rect if needed
        rectangles[rt] = isInMoveState;
        if (isInMoveState)
        {
            currMovingRT = rt;
        }
        return true;
    }

    //Returns true iff there is a canvas in motion
    public bool inMotion()
    {
        return currMovingRT != null;
    }

    public bool isGameObjectInMotion(GameObject go)
    {
        if(go == null)
        {
            return false;
        }
        if (!inMotion())
        {
            return false;
        }
        return go.transform.IsChildOf(currMovingRT.transform) || currMovingRT.transform.IsChildOf(go.transform);
    }

    //Use this to remove a RectTransform from contention
    public bool removeFromDetector(RectTransform rt, bool force = false)
    {
        if(rt == null)
        {
            return false;
        }
        if (inMotion() && (!force))
        {
            //Don't allow anything to be taken out while a canvas is moving - rethink?
            return false;
        }
        if(!rectangles.ContainsKey(rt)){
            //Attempted to remove a RectTransform not in contention
            return false;
        }

        rectangles.Remove(rt);
        if(rt == currMovingRT)
        {
            currMovingRT = null;
        }
        return true;
    }

    //Attempts to set the current moving rectangle to rt
    //Returns false if there is already a moving rectangle
    public bool setMovingRect(RectTransform rt)
    {
        if(rt == null)
        {
            return false;
        }
        if (inMotion())
        {
            return false;
        }
        if (!rectangles.ContainsKey(rt))
        {
            //Debug.Log("Given RectTransform does not already exist in list of tracked RTs");
            return false;
        }
        currMovingRT = rt;
        rectangles[rt] = true;
        return true;
    }

    //To be done when attempting to place a canvas - check if overlapping first
    //Using this lets Toggle know if an overlap is ocurring
    public bool clearMovingRect(RectTransform rt)
    {
        if(rt == null)
        {
            throw new System.Exception("Attempted to place a null RT");
        }
        if (!object.ReferenceEquals(rt, currMovingRT))
        {
            //Debug.Log("Attempted to clear with a rectangle not the currently moving one");
            return false;
        }
        if (!rectangles.ContainsKey(rt))
        {
            //Debug.Log("Given RectTransform does not already exist in list of tracked RTs");
            return false;
        }
        if (isOverlapping(currMovingRT))
        {
            errorTextManager.ErrorAlert("Cannot place window on top of another window");
            return false;
        }
        if (!inMotion())
        {
            return false;
        }
        currMovingRT = null;
        return true;
    }

    // Use this for initialization
    void Start()
    {
        //Init basic values
        currMovingRT = null;
        rectangles = new Hashtable();
    }

    //Checks if any canvas is overlapping with the argument
    private bool isOverlapping(RectTransform rt)
    {
        if (rt == null) { return false; }
        foreach (DictionaryEntry d in rectangles)
        {
            RectTransform s = d.Key as RectTransform;
            if (object.ReferenceEquals(rt, s)) //Don't check if a rectangle collides with itself
            {
                continue;
            }
            if (overlap(rt, s))
            {
                return true;
            }
        }
        return false;
    }

    //Detect if two rectangles are overlapping
    private bool overlap(RectTransform r1, RectTransform r2)
    {
        if (r1 == null || r2 == null || object.ReferenceEquals(r1, r2)) { return false; }
        //Get corners in world coordinates
        Vector3[] r1_wcorners = new Vector3[4];
        Vector3[] r2_wcorners = new Vector3[4];
        r1.GetWorldCorners(r1_wcorners);
        r2.GetWorldCorners(r2_wcorners);
        //Convert to screen coordinates
        Vector3[] r1_scorners = new Vector3[4];
        Vector3[] r2_scorners = new Vector3[4];
        r1_scorners[BOTTOMLEFT] = cam.WorldToScreenPoint(r1_wcorners[BOTTOMLEFT]);
        r2_scorners[BOTTOMLEFT] = cam.WorldToScreenPoint(r2_wcorners[BOTTOMLEFT]);
        r1_scorners[TOPRIGHT] = cam.WorldToScreenPoint(r1_wcorners[TOPRIGHT]);
        r2_scorners[TOPRIGHT] = cam.WorldToScreenPoint(r2_wcorners[TOPRIGHT]);
        //Now actually calculate if there is an intersection
        Rect a = new Rect(r1_scorners[BOTTOMLEFT].x, r1_scorners[BOTTOMLEFT].y,
                r1_scorners[TOPRIGHT].x - r1_scorners[BOTTOMLEFT].x, r1_scorners[TOPRIGHT].y - r1_scorners[BOTTOMLEFT].y);
        Rect b = new Rect(r2_scorners[BOTTOMLEFT].x, r2_scorners[BOTTOMLEFT].y,
                r2_scorners[TOPRIGHT].x - r2_scorners[BOTTOMLEFT].x, r2_scorners[TOPRIGHT].y - r2_scorners[BOTTOMLEFT].y);
        return a.Overlaps(b);
    }


    //Says whether a RectTransform being brought forwards would result in an overlap
    private bool translatedCloserWouldOverlap(RectTransform currPushedBackIn, RectTransform passiveRect)
    {
        //Placeholder for safety
        if(currPushedBackIn == null || passiveRect == null)
        {
            return false;
        }
        RectTransform currPushedBack = new RectTransform
        {
            anchoredPosition = currPushedBackIn.anchoredPosition,
            anchoredPosition3D = currPushedBackIn.anchoredPosition3D,
            anchorMax = currPushedBackIn.anchorMax,
            anchorMin = currPushedBackIn.anchorMin,
            offsetMax = currPushedBackIn.offsetMax,
            offsetMin = currPushedBackIn.offsetMin,
            pivot = currPushedBackIn.pivot,
            localPosition = currPushedBackIn.position,
            sizeDelta = currPushedBackIn.sizeDelta
        };
        currPushedBack.position += Camera.main.transform.forward * MOVEDIST;
        return overlap(currPushedBack, passiveRect);
    }

    private bool pushRT(RectTransform r, bool moveDirection)
    {
        if (r == null)
        {
            //Debug.Log("Unexpected null reference: CollisionDetector::pushRT");
            return false;
        }
        //Bit of a misnomer - BACKWARDS, as in further from the user, is actually forwards in the camera direction
        int dir = (moveDirection == FORWARDS) ? -1 : 1;
        r.position += Camera.main.transform.forward * dir * MOVEDIST;
        return true;
    }

    private void checkMovingCanvases()
    {
        //If a canvas overlaps with the currently moving canvas, then see if its bool is true, i.e. it has been pushed back
        //If it is, do nothing. If it is not, set its bool to true, and move it backwards.
        //If it is not overlapping and its bool is true, set its bool to false and move it back forwards.

        //No point checking if nothing is moving - assumes nothing overlaps at start
        if (!inMotion()) { return; }

        List<RectTransform> rectsToPushBack = new List<RectTransform>();
        List<RectTransform> rectsToPushForwards = new List<RectTransform>();
        foreach (DictionaryEntry d in rectangles)
        {
            RectTransform s = d.Key as RectTransform;
            if (object.ReferenceEquals(currMovingRT, s)) { continue; }
            if (overlap(currMovingRT, s))
            {
                if (!(bool)d.Value)
                {
                    //If it hasn't already been pushed back...
                    //push back canvas and set var
                    rectsToPushBack.Add(s);
                }
            }
            else
            {
                //canvases are not overlapping (anymore)
                //If it's pushed back, bring it back to the fold
                //Also check to see if bringing it back fowards would result in an overlap
                if ((bool)d.Value && (!translatedCloserWouldOverlap(currMovingRT, s)))
                {
                    rectsToPushForwards.Add(s);
                }
            }
        }
        foreach(RectTransform rt in rectsToPushBack)
        {
            rectangles[rt] = true;
            pushRT(rt, BACKWARDS);
        }
        foreach(RectTransform tr in rectsToPushForwards)
        {
            rectangles[tr] = false;
            pushRT(tr, FORWARDS);
        }
    }

    /*public void checkCollisionsWithStartWindow()
    {

    }*/
    
    // Update is called once per frame
    //Commented out to take out depth
    /*
    void Update()
    {
        checkMovingCanvases();
        if(bottomWindow != null && bottomWindow.activeInHierarchy) 
        {
            checkBottomWindow();
        }
    }
    */

}
