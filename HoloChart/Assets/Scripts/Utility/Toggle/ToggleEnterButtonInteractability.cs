﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleEnterButtonInteractability : MonoBehaviour 
{
    public Text inputFieldText; 
    public Button buttonScript; 

	public void Update() 
    {
        if (!buttonScript.interactable && inputFieldText.text.Length != 0)
        {
            buttonScript.interactable = true;
        }
        else if (buttonScript.interactable && inputFieldText.text.Length == 0)
        {
            buttonScript.interactable = false;
        }
	}
}
