﻿using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;

public class ToggleTagalong : MonoBehaviour
{
    public SimpleTagalong tagalong;
    private readonly Color32 highlightColor = new Color32(141, 133, 199, 255); 

    //Same reference throughout program
    public CollisionDetector de;
    //Should refer to the RectTransform of the parent
    public RectTransform rect;

    public AudioSource colllidingWindowsErrorSound;
    public AudioSource moveWindowSound;
    public AudioSource placeWindowSound;

    public bool IsInMotion()
    {
        return tagalong.enabled;
    }

    public void ToggleTagAlong()
    {
        if (tagalong.enabled)
        {
            //Only allow placing if no overlapping
            if (de.clearMovingRect(rect))
            {
                placeWindowSound.Play();
                tagalong.enabled = false;
                gameObject.GetComponent<Image>().color = Color.white;
            }
            else
            {
                colllidingWindowsErrorSound.Play();
            }
        }
        else
        {
            de.setMovingRect(rect);
            moveWindowSound.Play();
            tagalong.enabled = true;
            gameObject.GetComponent<Image>().color = highlightColor;
        }
    }

}
