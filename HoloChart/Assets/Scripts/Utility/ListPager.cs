﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListPager : MonoBehaviour {

    public List<List<GameObject>> windows;
    private int windowIndex = 0;
    public GameObject grid;
    public GameObject rightButton, leftButton;
    public Text currentPageNumber;
    public AudioSource pageLeftSound;
    public AudioSource pageRightSound;

    public void InitializePager()
    {
        if (windows.Count == 0)
        {
            leftButton.SetActive(false);
            rightButton.SetActive(false);
            return;
        }

        AttachAllObjectstoGridAndMakeInactive();
        SetList();
        UpdatePageNumber();
    }

    private void AttachAllObjectstoGridAndMakeInactive()
    {
        foreach (List<GameObject> listGameObjects in windows)
        {
            foreach (GameObject gameObject in listGameObjects)
            {
                gameObject.transform.SetParent(grid.transform, false);
                gameObject.SetActive(false);
            }
        }
    }

    public void PageRight()
    {
        pageRightSound.Play();
        foreach (GameObject gameObject in windows[windowIndex])
        {
            gameObject.SetActive(false);
        }

        ++windowIndex;
        SetList();
        UpdatePageNumber();
    }

    public void PageLeft()
    {
        pageLeftSound.Play();
        foreach (GameObject gameObject in windows[windowIndex])
        {
            gameObject.SetActive(false);
        }

        --windowIndex;
        SetList();
        UpdatePageNumber();
    }

    private void SetList()
    {
        foreach (GameObject gameObject in windows[windowIndex])
        {
            gameObject.SetActive(true);
        }

        leftButton.SetActive(windowIndex != 0);
        rightButton.SetActive(windowIndex != (windows.Count - 1));
    }

    private void UpdatePageNumber() 
    {
        currentPageNumber.text = windowIndex + 1 + " of " + windows.Count;
    }
}
