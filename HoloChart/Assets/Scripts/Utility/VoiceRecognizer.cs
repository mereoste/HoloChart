using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

[System.Serializable]
public class VoiceRecognizer : MonoBehaviour
{
    KeywordRecognizer keywordrecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    public WindowManager helpWindowManager;
    public GameObject startWindow;
    public GameObject controlWindow;
    public GameObject patientWindow;
    public GameObject diagnosticWindow;
    public Button logoutButton;
    public DiagnosticWindowTransitions diagnosticWindowTransitions;
    public GameObject summaryPanel;
    public GameObject symptomsPanel;
    public GameObject diagnosesPanel;
    public PatientWindowManager patientWindowManager;
    public ErrorTextManager errorTextManager;

    public CollisionDetector collisionDetector;

    void Start()
    {
        keywords.Add("help", () => {
            if (collisionDetector.inMotion())
            {
                errorTextManager.ErrorAlert("Cannot open help when window in motion");
                return;
            }
            else if (!startWindow.activeSelf)
            {
                helpWindowManager.ButtonClick();
            }
        });
        keywords.Add("close", () =>
        {
            if (collisionDetector.inMotion())
            {
                errorTextManager.ErrorAlert("Cannot open help when window in motion");
                return;
            }
            try
            {
                GameObject focusedObject = GetFocusedGameObject();
                WindowObject window = focusedObject.transform.root.GetComponentInChildren<WindowObject>();
                if (window == null)
                {
                    throw new Exception("Window cannot be closed");
                }
                window.CloseWindow();
            }
            catch (Exception e)
            {
                errorTextManager.ErrorAlert(e.Message);
            }
        });
        keywords.Add("closeall", () =>
        {
            patientWindowManager.CloseLabReports();
        });
        keywords.Add("move", () =>
        {
            try
            {
                GameObject focusedObject = GetFocusedGameObject();
                ToggleTagalong toggleTagalong = focusedObject.transform.root.GetComponentInChildren<ToggleTagalong>();
                if (toggleTagalong == null)
                {
                    throw new Exception("Window cannot be moved");
                }
                if (!toggleTagalong.IsInMotion())
                {
                    toggleTagalong.ToggleTagAlong();
                }
                else
                {
                    throw new Exception("Window is already moving");
                }
            }
            catch (Exception e)
            {
                errorTextManager.ErrorAlert(e.Message);
            }
        });
        keywords.Add("place", () =>
        {
            try
            {
                GameObject focusedObject = GetFocusedGameObject();
                ToggleTagalong toggleTagalong = focusedObject.transform.root.GetComponentInChildren<ToggleTagalong>();
                if (toggleTagalong == null)
                {
                    throw new Exception("Window cannot be placed");
                }
                if (toggleTagalong.IsInMotion())
                {
                    toggleTagalong.ToggleTagAlong();
                }
                else
                {
                    throw new Exception("Window must be moving");
                }
            }
            catch (Exception e)
            {
                errorTextManager.ErrorAlert(e.Message);
            }
        });
        keywords.Add("logout", () => {
            if (collisionDetector.inMotion())
            {
                errorTextManager.ErrorAlert("Cannot logout when window in motion");
                return;
            }
            if (controlWindow.activeSelf)
            {
                logoutButton.onClick.Invoke();
            }
        });
        keywords.Add("summary", () =>
        {
            if (diagnosticWindow.activeSelf)
            {
                diagnosticWindowTransitions.OpenSummaryPanel();
            }
        });
        keywords.Add("symptoms", () =>
        {
            if (diagnosticWindow.activeSelf)
            {
                diagnosticWindowTransitions.OpenSymptomsPanel();
            }
        });
        keywords.Add("diagnoses", () =>
        {
            if (diagnosticWindow.activeSelf)
            {
                diagnosticWindowTransitions.OpenDiagnosesPanel();
            }
        });

        keywordrecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordrecognizer.OnPhraseRecognized += KeywordRecognizerOnPhraseRecognized;
        keywordrecognizer.Start();
    }

    private GameObject GetFocusedGameObject()
    {
        GameObject focusedObject = HoloToolkit.Unity.InputModule.GazeManager.Instance.HitObject;
        if (focusedObject == null)
        {
            throw new Exception("No focused object");
        }
        return focusedObject;
    }

    void KeywordRecognizerOnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;

        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
}