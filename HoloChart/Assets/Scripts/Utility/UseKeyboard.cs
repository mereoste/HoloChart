﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.UI;

public class UseKeyboard : MonoBehaviour, IInputClickHandler
{
    TouchScreenKeyboard keyboard;
    public Text inputText;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, false, false, "");
    }
}
