﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour {

    public CollisionDetector collisionDetector;
    public GameObject windowPrefab;
    public AudioSource collidingErrorSound, placeWindowSound, moveWindowSound;
    protected readonly Color32 highlightColor = new Color32(141, 133, 199, 255);
    private GameObject window = null;

    public void Reset()
    {
        if (window != null)
        {
            ButtonClick();
        }
    }

    public void ButtonClick()
    {
        if (window == null)
        {
            try
            {
                window = CreateWindow();
                InitializeWindow();
                gameObject.GetComponent<Image>().color = highlightColor;
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }
        }
        else
        {
            CloseWindow();
        }
    }

    private void InitializeWindow()
    {
        collisionDetector.addToDetector(window.GetComponent<RectTransform>(), true);
        WindowObject windowScript = window.GetComponent<WindowObject>();
        windowScript.windowManager = this;
        windowScript.toggleTagalong.de = collisionDetector;
        windowScript.toggleTagalong.colllidingWindowsErrorSound = collidingErrorSound;
        windowScript.toggleTagalong.moveWindowSound = moveWindowSound;
        windowScript.toggleTagalong.placeWindowSound = placeWindowSound;
    }

    public void CloseWindow()
    {
        collisionDetector.removeFromDetector(window.GetComponent<RectTransform>());
        Destroy(window);
        window = null;
        gameObject.GetComponent<Image>().color = Color.white;
    }

    public virtual GameObject CreateWindow()
    {
        return Instantiate(windowPrefab);
    }
}
