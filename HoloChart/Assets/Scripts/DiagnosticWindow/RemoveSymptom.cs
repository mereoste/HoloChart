﻿using UnityEngine;

public class RemoveSymptom : MonoBehaviour
{
    public string symptomIdentifier;

    public void RemoveSelf()
    {
        gameObject.GetComponentInParent<DiagnosticWindowStateManager>().DeleteSymptom(symptomIdentifier);
    }
}
