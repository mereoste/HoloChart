﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryUIManager : MonoBehaviour {
    public DiagnosticWindowStateManager stateManager;
    public Image symptomsGrid;
    public GameObject symptomPrefab;
    public Image diagnosesGrid;
    public GameObject diagnosisPrefab;

    private Dictionary<string, GameObject> symptoms;
    private List<GameObject> messages;

    public void Awake()
    {
        symptoms = new Dictionary<string, GameObject>();
        messages = new List<GameObject>();
    }

    public void AddDiagnosis(Diagnosis diagnosis)
    {
        GameObject diagnosisObject = Instantiate(diagnosisPrefab);
        Text diagnosisName = diagnosisObject.GetComponent<Text>();
        diagnosisName.text += diagnosis.name;
        diagnosisName.transform.SetParent(diagnosesGrid.transform, false);
    }

    public void DisplaySymptoms()
    {
        ClearSymptoms();
        foreach (Symptom symptom in stateManager.patientSymptoms)
        {
            AddSymptom(symptom);
        }
    }

    public void ClearSymptoms()
    {
        foreach (GameObject gameObject in symptoms.Values)
        {
            Destroy(gameObject);
        }
        symptoms.Clear();
    }

    public void AddSymptom(Symptom symptom)
    {
        GameObject symptomObject = Instantiate(symptomPrefab);
        symptoms[symptom.id] = symptomObject;
        Text symptomName = symptomObject.GetComponent<Text>();
        symptomName.text += symptom.name;
        symptomObject.transform.SetParent(symptomsGrid.transform, false);
    }
	
    public void RemoveSymptom(string symptomIdentifier)
    {
        Destroy(symptoms[symptomIdentifier]);
        symptoms.Remove(symptomIdentifier);

        if (symptoms.Count == 0)
        {
            DisplayNoSymptomsAvailable();
        }
    }
	
    public void ClearDiagnoses() 
    {
        foreach (Transform diagnosis in diagnosesGrid.transform)
        {
            Destroy(diagnosis.gameObject);
        }
    }

    public void AddSymptomMessage(string message)
    {
        GameObject messageObject = Instantiate(symptomPrefab);
        Text messageText = messageObject.GetComponent<Text>();
        messageText.text = message;
        messageObject.transform.SetParent(symptomsGrid.transform, false);
        messages.Add(messageObject);
    }

    public void AddDiagnosesMessage(string message)
    {
        GameObject messageObject = Instantiate(diagnosisPrefab);
        Text messageText = messageObject.GetComponent<Text>();
        messageText.text = message;
        messageObject.transform.SetParent(diagnosesGrid.transform, false);
    }

    public void ClearMessages()
    {
        messages.ForEach(Destroy);
        messages.Clear();
    }

    private void DisplayNoSymptomsAvailable()
    {
        AddSymptomMessage("No symptoms available for patient");
        AddSymptomMessage("Please click on symptom window to add symptoms");
    }
}
