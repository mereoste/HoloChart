﻿using UnityEngine;


public static class VitalColoring
{
    public static readonly Color DANGER = new Color32(145, 0, 0, 255);
    public static readonly Color WARNING = new Color32(255, 250, 0, 255);
    public static readonly Color SAFE = Color.white;

    public static Color RespiratoryRate(int age, int respRate) 
    {
        /*
         * taken crudely from here
         * https://emedicine.medscape.com/article/2172054-overview#a1
         */

        if (age < 1) {
            if (respRate < 20 || respRate > 50) {
                return WARNING;
            }
        }
        else if (age >= 1 && age < 10) {
            if (respRate < 15 || respRate > 30) {
                return WARNING; 
            }
        }
        else if (age >= 10 && age < 20) {
            if (respRate < 12 || respRate > 30) {
                return WARNING; 
            }
        }
        else {
            if (respRate < 16 || respRate > 80) {
                return WARNING;
            }
        }
        return SAFE;
    }

    public static Color HeartRate(int age, int heartRate) 
    {
        /*
         * taken crudely from here
         * https://emedicine.medscape.com/article/2172054-overview#a1
         */

        if (age < 1)
        {
            if (heartRate < 80 || heartRate > 160)
            {
                return WARNING;
            }
        }
        else if (age >= 1 && age < 10)
        {
            if (heartRate < 70 || heartRate > 130)
            {
                return WARNING;
            }
        }
        else if (age >= 10 && age < 20)
        {
            if (heartRate < 60 || heartRate > 110)
            {
                return WARNING;
            }
        }
        else
        {
            if (heartRate < 50 || heartRate > 80)
            {
                return WARNING;
            }
        }
        return SAFE;
    }


    public static Color BloodPressure(int systolic, int diastolic)
    {
        // according to NHLBI guidelines

        if (systolic >= 140 || diastolic >= 90)
        {
            return DANGER;
        }
        else if ((systolic >= 120 && systolic < 140) ||
                 (diastolic >= 80 && diastolic < 90))
        {
            return WARNING;
        }
        return SAFE;
    }
}
