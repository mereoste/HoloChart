﻿using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class DiagnosticWindowStateManager : MonoBehaviour {

    public SummaryUIManager summaryUI;
    public SymptomsUIManager symptomsUI;
    public DiagnosesUIManager diagnosesUI;

    public AudioSource addSymptomSound, removeSymptomSound;

    public Text inputSymptomText;
    public DiagnosticClient diagnosticClient;
    public List<Symptom> patientSymptoms;
    public List<Diagnosis> currentDiagnoses;
    public const int MAX_SYMPTOMS = 5, MAX_DIAGNOSES = 8;
    private int patientAge;
    private string patientSex;
    private string lastCompleteQuery;
    
	public void Awake()
    {
        patientSymptoms = new List<Symptom>();
        currentDiagnoses = new List<Diagnosis>();
    }


    public void InitPatientSymptoms(List<string> patientSymptomsFreeText, int age, string sex)
    {
        List<Symptom> oldSymptoms = new List<Symptom>(patientSymptoms);
        int oldAge = patientAge;
        string oldSex = patientSex;

        patientAge = age;
        patientSex = sex;
        lastCompleteQuery = "";

        patientSymptoms.Clear();
        try
        {
            string freeTextSymptoms = String.Join(". ", patientSymptomsFreeText.ToArray());
            AddSymptomsToInternalSymptomsList(freeTextSymptoms);
            if(patientSymptoms.Count == 0)
            {
                DisplayNoSymptomsOrDiagnosesAvailableMessages();
                return;
            }
            UpdateInternalDiagnosesList();
            symptomsUI.DisplaySymptoms();
            diagnosesUI.DisplayDiagnoses();
        }
        catch (WebException e)
        {
            patientSymptoms = oldSymptoms;
            patientAge = oldAge;
            patientSex = oldSex;
            throw e;
        }
    }

    public void AddUserInputtedSymptoms()
    {
        if (inputSymptomText.text.Equals(lastCompleteQuery))
        {
            symptomsUI.DisplayErrorFeedback("Symptom(s) already processed!");
            return;
        }

        List<Symptom> oldSymptoms = new List<Symptom>(patientSymptoms);
        try
        {
            symptomsUI.DisplayLoadingMessage();
            try
            {
                AddSymptomsToInternalSymptomsList(inputSymptomText.text);
            }
            catch (Exception e)
            {
                symptomsUI.DisplayErrorFeedback(e.Message);
                lastCompleteQuery = inputSymptomText.text;
                return;
            }
            if (patientSymptoms.Count == oldSymptoms.Count)
            {
                symptomsUI.DisplayErrorFeedback("No symptoms found for given text");
                lastCompleteQuery = inputSymptomText.text;
                return;
            }
            UpdateInternalDiagnosesList();
            lastCompleteQuery = inputSymptomText.text;
            symptomsUI.DisplaySymptoms();
            diagnosesUI.DisplayDiagnoses();
            symptomsUI.DisplayDefaultFeedback();
            inputSymptomText.text = "";
            addSymptomSound.Play();
        }
        catch (WebException e)
        {
            Debug.Log(e);
            patientSymptoms = oldSymptoms;
            symptomsUI.DisplayErrorFeedback("Internet connection failed");
        }
    }

    public void DeleteSymptom(string symptomIdentifier)
    {
        List<Symptom> oldSymptoms = new List<Symptom>(patientSymptoms);
        try
        {
            symptomsUI.DisplayLoadingMessage();
            patientSymptoms.RemoveAll(symptom => symptom.id.Equals(symptomIdentifier));
            UpdateInternalDiagnosesList();
            symptomsUI.RemoveSymptom(symptomIdentifier);
            symptomsUI.DisplayDefaultFeedback();
            diagnosesUI.DisplayDiagnoses();
            if (patientSymptoms.Count == 0)
            {
                DisplayNoSymptomsOrDiagnosesAvailableMessages();
            }
            lastCompleteQuery = ""; // do this in case user accidentally deletes symptom they have just added
            removeSymptomSound.Play();
        }
        catch (WebException e)
        {
            Debug.Log(e);
            symptomsUI.DisplayErrorFeedback("Internet connection failed");
            patientSymptoms = oldSymptoms;
        }

    }

    private void AddSymptomsToInternalSymptomsList(string freeTextSymptoms)
    {
        List<Symptom> symptoms = diagnosticClient.GetSymptomsFromFreeSpeech(freeTextSymptoms);
        bool duplicatesFound = false;
        foreach (Symptom symptom in symptoms)
        {
            if (patientSymptoms.Count == MAX_SYMPTOMS)
            {
                break; 
            }

            if (patientSymptoms.TrueForAll(sym => !sym.id.Equals(symptom.id)))
            {
                patientSymptoms.Add(symptom);
            }
            else
            {
                duplicatesFound = true;
            }
        }

        if (duplicatesFound)
        {
            throw new Exception("Symptom(s) have already been added");
        }
    }

    private void UpdateInternalDiagnosesList()
    {
        List<string> symptomIds = new List<string>();
        foreach (Symptom symptom in patientSymptoms)
        {
            symptomIds.Add(symptom.id);
        }
        currentDiagnoses = diagnosticClient.GetDiagnoses(symptomIds, patientAge, patientSex);
        for (int i = 0; i < currentDiagnoses.Count; ++i)
        {
            if (i > MAX_DIAGNOSES)
            {
                break;
            }
        }
    }

    private void DisplayNoSymptomsOrDiagnosesAvailableMessages()
    {
        summaryUI.AddSymptomMessage("No symptoms available");
        summaryUI.AddDiagnosesMessage("No diagnoses available");
    }
}
