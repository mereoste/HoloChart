﻿using UnityEngine;

public class DiagnosticWindowTransitions : MonoBehaviour {
    public GameObject summaryPanel, symptomsPanel, diagnosesPanel, backButtonObject;
    public SymptomsUIManager symptomsUI;

    public void OpenSummaryPanel()
    {
        if (summaryPanel.activeSelf)
        {
            return;
        }

        symptomsPanel.SetActive(false);
        diagnosesPanel.SetActive(false);
        backButtonObject.SetActive(false);
        summaryPanel.SetActive(true);
    }

    public void OpenDiagnosesPanel()
    {
        if (diagnosesPanel.activeSelf)
        {
            return;
        }

        symptomsPanel.SetActive(false);
        summaryPanel.SetActive(false);
        backButtonObject.SetActive(true);
        diagnosesPanel.SetActive(true);
    }

    public void OpenSymptomsPanel()
    {
        if (symptomsPanel.activeSelf)
        {
            return;
        }

        symptomsUI.DisplayDefaultFeedback();
        summaryPanel.SetActive(false);
        diagnosesPanel.SetActive(false);
        backButtonObject.SetActive(true);
        symptomsPanel.SetActive(true);
    }
}
