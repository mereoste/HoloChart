﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SymptomsUIManager : MonoBehaviour
{
    public DiagnosticWindowStateManager stateManager;
    public SummaryUIManager summaryUIManager;

    public Text feedbackText;
    public Image grid;
    public GameObject prefab;
    public Button enterButton; 
    private Dictionary<string, GameObject> symptomObjects;
    private readonly string defaulFeedback = DiagnosticWindowStateManager.MAX_SYMPTOMS + " symptoms can be displayed";

    public void Awake()
    {
        symptomObjects = new Dictionary<string, GameObject>();
        DisplayDefaultFeedback();
    }

    public void DisplaySymptoms()
    {
        summaryUIManager.ClearMessages();
        ClearSymptomsFromWindows();
        foreach (Symptom symptom in stateManager.patientSymptoms)
        {
            AddSymptom(symptom);
            summaryUIManager.AddSymptom(symptom);
        }
    }

    private void ClearSymptomsFromWindows()
    {
        foreach (GameObject gameObject in symptomObjects.Values)
        {
            Destroy(gameObject);
        }
        symptomObjects.Clear();
        summaryUIManager.ClearSymptoms();
    }

    private void AddSymptom(Symptom symptom)
    {
        GameObject symptomObject = Instantiate(prefab);
        symptomObjects[symptom.id] = symptomObject;

        Text[] symptomText = symptomObject.GetComponentsInChildren<Text>();
        symptomText[0].text = symptom.name;

        symptomObject.GetComponent<RemoveSymptom>().symptomIdentifier = symptom.id;
        symptomObject.transform.SetParent(grid.transform, false);

        enterButton.interactable |= stateManager.patientSymptoms.Count >= DiagnosticWindowStateManager.MAX_SYMPTOMS;
    }

    public void RemoveSymptom(string symptomIdentifier)
    {
        enterButton.interactable |= stateManager.patientSymptoms.Count <
            DiagnosticWindowStateManager.MAX_SYMPTOMS;

        Destroy(symptomObjects[symptomIdentifier]);
        symptomObjects.Remove(symptomIdentifier);
        summaryUIManager.DisplaySymptoms();
    }

    public void DisplayDefaultFeedback()
    {
        feedbackText.text = defaulFeedback;
        feedbackText.color = Color.white;
    }

    public void DisplayErrorFeedback(string errorMessage)
    {
        feedbackText.text = errorMessage;
        feedbackText.color = Color.red;
    }

    public void DisplayLoadingMessage()
    {
        feedbackText.text = "Loading...";
        feedbackText.color = Color.white;
    }
}
