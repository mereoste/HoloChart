﻿
public class Symptom
{
    public string id;
    public string name;
    public string commonName; 

	public Symptom(string id, string name, string commonName)
	{
        this.id = id;
        this.name = name;
        this.commonName = commonName;
	}
}
