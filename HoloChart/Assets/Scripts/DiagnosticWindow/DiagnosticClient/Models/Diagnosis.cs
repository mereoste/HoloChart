﻿
public class Diagnosis
{
    public string id;
    public string name;
    public string commonName;
    public string probability; 

    public Diagnosis(string id, string name, string commonName, string probability) 
    {
        this.id = id;
        this.name = name;
        this.commonName = commonName;
        this.probability = probability;
    }
}


