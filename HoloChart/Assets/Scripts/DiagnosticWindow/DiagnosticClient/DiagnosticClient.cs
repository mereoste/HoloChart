﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using System.Text;
using System.Net;
using System.Diagnostics;


public class DiagnosticClient : MonoBehaviour
{
    private int webTimeOutSeconds = 5;
    public void Awake() 
    {
        allSymptoms = new List<Symptom>(); 
    }

    public List<Diagnosis> GetDiagnoses(List<string> symptomIds, int age, string sex)
    {
        string jsonFormatedSymptoms = FormatSymptomsForRequest(symptomIds, age, sex);
        string jsonResponse = SendPostRequest("diagnosis", jsonFormatedSymptoms);
        return ParseDiagnoses(jsonResponse);
    }

    public List<Symptom> GetSymptomsFromFreeSpeech(string freeSpeech)
    {
        string jsonFormatedText = "{\"text\": \"" + freeSpeech + "\"}";
        string jsonResponse = SendPostRequest("parse", jsonFormatedText);
        return ParseFreeSpeechSymptoms(jsonResponse); 
    }

    public List<Symptom> GetAllSymptoms()
    {
        if (this.allSymptoms.Count > 0) {
            return this.allSymptoms; 
        }
        string jsonResponse = SendGetRequest("symptoms");
        ParseSymptoms(jsonResponse);
        return this.allSymptoms; 
    }

    private string FormatSymptomsForRequest(List<string> symptomIDs, int age, string sex)
    {
        string jsonFormatedSymptoms = "{\n  \"sex\": \"" + sex.ToLower() + "\",\n " +
            " \"age\": " + age + ",\n  " + "\"evidence\": [ \n";

        for (int i = 0; i < symptomIDs.Count; ++i) {
            jsonFormatedSymptoms += "{\n \"id\": \"" + symptomIDs[i] +
                "\",\n \"choice_id\": \"present\"\n }";

            if (i != symptomIDs.Count - 1) {
                jsonFormatedSymptoms += ", ";
            }
            jsonFormatedSymptoms += "\n";
        }
        return jsonFormatedSymptoms += "]\n}";
    }

    private string SendPostRequest(string endpoint, string jsonData)
    {
        var myUrl = this.url + "/" + endpoint;
        using (UnityWebRequest request = UnityWebRequest.Post(myUrl, UnityWebRequest.kHttpVerbPOST))
        {
            request.SetRequestHeader("app-key", appKey);
            request.SetRequestHeader("app-id", appID);
            request.SetRequestHeader("content-type", "application/json");

            byte[] bytes = Encoding.UTF8.GetBytes(jsonData);
            UploadHandlerRaw uploadHandler = new UploadHandlerRaw(bytes);
            request.uploadHandler = uploadHandler;

            request.SendWebRequest();

            Stopwatch watch = new Stopwatch();
            while (!request.isDone) {
                if (watch.Elapsed.Seconds > webTimeOutSeconds) {
                    throw new WebException("Request timed out");
                }
            }

            if (request.isNetworkError) {
                throw new WebException(request.error);
            }

            string response = request.downloadHandler.text;

            if (response.Contains("invalid json"))
            {
                throw new WebException("Invalid JSON");
            }

            return request.downloadHandler.text;
        }
    }

    public List<Diagnosis> ParseDiagnoses(string jsonString)
    {
        var jsonData = JSON.Parse(jsonString);
        List<Diagnosis> result = new List<Diagnosis>();
        foreach (JSONNode diagnosis in jsonData["conditions"].AsArray) {
            result.Add(new Diagnosis(diagnosis["id"], diagnosis["name"], 
                                     diagnosis["common_name"], diagnosis["probability"]));
        }
        return result; 
    }
   
    private string SendGetRequest(string endpoint)
    {
        var myUrl = this.url + "/" + endpoint;

        using (UnityWebRequest request = UnityWebRequest.Get(myUrl))
        {
            request.SetRequestHeader("app-key", appKey);
            request.SetRequestHeader("app-id", appID);

            request.SendWebRequest();  

            Stopwatch watch = new Stopwatch();
            while (!request.isDone)
            {
                if (watch.Elapsed.Seconds > webTimeOutSeconds) {
                    throw new WebException("Request timed out");
                }
            }

            if (request.isNetworkError) {
                throw new WebException(request.error);
            }
            return request.downloadHandler.text;
        }
    }

    public void ParseSymptoms(string jsonString)
    {
        var jsonData = JSON.Parse(jsonString);
        foreach (JSONNode symptom in jsonData.AsArray) {
            this.allSymptoms.Add(new Symptom(symptom["id"],
                                          symptom["name"], symptom["common_name"]));
        }
    }

    public List<Symptom> ParseFreeSpeechSymptoms(string jsonString)
    {
        var jsonData = JSON.Parse(jsonString);
        List<Symptom> mentionedSymptoms = new List<Symptom>();
        foreach (JSONNode symptom in jsonData["mentions"].AsArray) {
            mentionedSymptoms.Add(new Symptom(symptom["id"],
                                              symptom["name"], symptom["common_name"]));
        }
        return mentionedSymptoms;
    }

    private List<Symptom> allSymptoms;
	private readonly string appKey = "768f6d45b1e3622e9575eefb1dc06689";
	private readonly string appID = "6f023bd4";
    private readonly string url = "https://api.infermedica.com/v2";
}
