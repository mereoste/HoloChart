﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class DiagnosesUIManager : MonoBehaviour
{
    public DiagnosticWindowStateManager stateManager;
    public SummaryUIManager summaryUIManager;

    public Text feedbackText;
    public Image grid;
    public GameObject prefab;

    private readonly string defaultFeedback = DiagnosticWindowStateManager.MAX_DIAGNOSES + " diagnoses can be displayed";

    public void Awake()
    {
        DisplayDefaultFeedback();
    }

    public void DisplayDiagnoses()
    {
        ClearDiagnosesFromWindows();
        List<Diagnosis> diagnoses = stateManager.currentDiagnoses;
        for (int i = 0; i < diagnoses.Count; ++i)
        {
            if (i > DiagnosticWindowStateManager.MAX_DIAGNOSES)
            {
                break;
            }
            summaryUIManager.AddDiagnosis(diagnoses[i]);
            AddDiagnosis(diagnoses[i]);
        }
    }

    private void ClearDiagnosesFromWindows()
    {
        summaryUIManager.ClearDiagnoses();
        foreach (Transform diagnosis in grid.transform)
        {
            Destroy(diagnosis.gameObject);
        }
    }

    private void AddDiagnosis(Diagnosis diagnosis)
    {
        GameObject diagnosisObject = Instantiate(prefab);

        Text[] diagnosesText = diagnosisObject.GetComponentsInChildren<Text>();
        diagnosesText[0].text = diagnosis.id;
        diagnosesText[1].text = diagnosis.name;


        NumberFormatInfo nfi = new CultureInfo("en-US").NumberFormat;
        Double d = Convert.ToDouble(diagnosis.probability); // hopefully converts correctly 
        diagnosesText[2].text = String.Format(d.ToString("P", nfi));

        diagnosisObject.transform.SetParent(grid.transform, false);
    }

    public void DisplayDefaultFeedback()
    {
        feedbackText.text = defaultFeedback;
        feedbackText.color = Color.white;
    }

    public void DisplayDiagnosesWindowFeedbackError(string errorMessage)
    {
        feedbackText.text = errorMessage;
        feedbackText.color = Color.red;
    }

}
