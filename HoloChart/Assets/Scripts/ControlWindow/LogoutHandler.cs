﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoutHandler : MonoBehaviour {
    public WindowManager helpWindowManager;
    public GameObject patientWindow;
    public GameObject diagnosticWindow;
    public GameObject startWindow;
    public GameObject controlWindow;
    public CollisionDetector de;
    public AudioSource logoutSound;
    public InputField symptomEntryText;
    public DiagnosticWindowTransitions diagnosticWindowTransitions;

    public void Logout()
    {
        helpWindowManager.Reset();
        patientWindow.GetComponent<PatientWindowManager>().CloseLabReports();
        diagnosticWindowTransitions.OpenSummaryPanel();
        de.removeFromDetector(patientWindow.GetComponent<RectTransform>());
        de.removeFromDetector(diagnosticWindow.GetComponent<RectTransform>());
        de.removeFromDetector(controlWindow.GetComponent<RectTransform>(), true);

        if (symptomEntryText != null)
        {
            symptomEntryText.Select();
            symptomEntryText.text = "";
        }

        diagnosticWindow.SetActive(false);
        patientWindow.SetActive(false);
        controlWindow.SetActive(false);
        startWindow.SetActive(true);

        logoutSound.Play();
    }
}
