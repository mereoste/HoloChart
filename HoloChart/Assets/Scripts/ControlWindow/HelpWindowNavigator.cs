﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpWindowNavigator : MonoBehaviour {

    private readonly Color32 HIGHLIGHTED_COLOR = new Color32(141, 133, 199, 255); 
    private readonly Color DEFAULT_COLOR = Color.white; 

    public GameObject GeneralWindowPanel;
    public GameObject VoiceCommandsWindowPanel;
    public GameObject PatientWindowPanel;
    public GameObject DiagnosticWindowPanel;

    public Button GeneralWindowHelpButton;
    public Button VoiceCommandsWindowHelpButton;
    public Button PatientWindowHelpButton;
    public Button DiagnosticWindowHelpButton;

    private Button HighlightedButton;

    void Start () {

        HighlightedButton = GeneralWindowHelpButton;

        GeneralWindowPanel.SetActive(true);
        VoiceCommandsWindowPanel.SetActive(false);
        PatientWindowPanel.SetActive(false);
        DiagnosticWindowPanel.SetActive(false);

        HighLightButton(GeneralWindowHelpButton);

        GeneralWindowHelpButton.onClick.AddListener(ActivateGeneralHelpWindow);
        VoiceCommandsWindowHelpButton.onClick.AddListener(ActivateVoiceCommandsHelpWindow);
        PatientWindowHelpButton.onClick.AddListener(ActivatePatientHelpWindow);
        DiagnosticWindowHelpButton.onClick.AddListener(ActivateDiagnosticHelpWindow);
	}

    void ActivateGeneralHelpWindow()
    {
        GeneralWindowPanel.SetActive(true);
        VoiceCommandsWindowPanel.SetActive(false);
        PatientWindowPanel.SetActive(false);
        DiagnosticWindowPanel.SetActive(false);

        HighLightButton(GeneralWindowHelpButton);
    }

    void ActivateVoiceCommandsHelpWindow()
    {
        GeneralWindowPanel.SetActive(false);
        VoiceCommandsWindowPanel.SetActive(true);
        PatientWindowPanel.SetActive(false);
        DiagnosticWindowPanel.SetActive(false);

        HighLightButton(VoiceCommandsWindowHelpButton);
    }

    void ActivatePatientHelpWindow()
    {
        GeneralWindowPanel.SetActive(false);
        VoiceCommandsWindowPanel.SetActive(false);
        PatientWindowPanel.SetActive(true);
        DiagnosticWindowPanel.SetActive(false);

        HighLightButton(PatientWindowHelpButton);
    }

    void ActivateDiagnosticHelpWindow()
    {
        GeneralWindowPanel.SetActive(false);
        VoiceCommandsWindowPanel.SetActive(false);
        PatientWindowPanel.SetActive(false);
        DiagnosticWindowPanel.SetActive(true);

        HighLightButton(DiagnosticWindowHelpButton);
    }

    private void HighLightButton(Button b) 
    {
        HighlightedButton.GetComponent<Image>().color = DEFAULT_COLOR;
        b.GetComponent<Image>().color = HIGHLIGHTED_COLOR;
        HighlightedButton = b;
    }
}
