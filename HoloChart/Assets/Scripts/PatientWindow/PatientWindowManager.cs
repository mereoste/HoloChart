﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PatientWindowManager : MonoBehaviour {

    private const string bulletPoint = "• ";

    // state
    public string mrn;
    private List<LabReportManager> labReports;
    
    public Text nameText, birthdayText;

    // demographics
    public Text ageText, sexText, heightText, weightText;
 
    // medications
    public Text medicationTextPrefab;
    public ListPager medicationPager;

    // allergies
    public Text allergyTextPrefab;
    public ListPager allergyPager;

    // lab reports
    public GameObject labReportButtonPrefab;
    public ListPager labreportPager;
    public CollisionDetector collisionDetector;
    public AudioSource collidingErrorSound, placeWindowSound, moveWindowSound;

    // vitals
    public Text recentVitalDateText;
    public Text recentBloodPressure, recentHeartRate;
    public Text recentRespitoryRate, recentPulseOximetry;
    public Text initialVitalDateText;
    public Text initialBloodPressure, initialHeartRate;
    public Text initialRespitoryRate, initialPulseOximetry;

    public void Awake()
    {
        labReports = new List<LabReportManager>();
    }

    public void CloseLabReports()
    {
        foreach(LabReportManager manager in labReports)
        {
            manager.Reset();
        }
    }

	public void PopulatePatientWindow(Patient patient)
    {
        // Destroy all components created for last patient
        ClearOldData();

        PopulateHeader(patient);
        PopulateDemographics(patient);
        PopulateAllergies(patient);
        PopulateMedications(patient);
        PopulateLabReports(patient);
        PopulateVitals(patient);

        mrn = patient.mrn;
    }

    private void ClearOldData()
    {
        ClearList(medicationPager);
        ClearList(allergyPager);
        ClearList(labreportPager);
    }

    private void ClearList(ListPager listPager)
    {
        if (listPager.windows == null)
        {
            return;
        }

        foreach (List<GameObject> gameObjects in listPager.windows)
        {
            foreach (GameObject gameObject in gameObjects)
            {
                Destroy(gameObject);
            }
        }

        listPager.windows = null;
    }

    private void PopulateHeader(Patient patient)
    {
        nameText.text = patient.name;
        DateTime birthday = patient.birthday;
        string month = birthday.ToString("MMMM");
        string day = birthday.Day + GetCountSuffix(birthday.Day);
        birthdayText.text = "Born " + month + " " + day + ", " + birthday.Year;
    }

    private string GetCountSuffix(int n)
    {
        if (n >= 11 && n <= 13)
        {
            return "th";
        }
        switch (n % 10)
        {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }

    private void PopulateDemographics(Patient patient)
    {
        DateTime now = DateTime.Today;
        int age = now.Year - patient.birthday.Year;
        if (now < patient.birthday.AddYears(age))
        {
            age--;
        }

        ageText.text = bulletPoint + age + " years old";
        sexText.text = bulletPoint + patient.sex;
        heightText.text = bulletPoint + patient.height_inches / 12 + "'" + patient.height_inches % 12 + "\"";
        weightText.text = bulletPoint + patient.weight_pounds + " lbs";
    }

    private void PopulateMedications(Patient patient)
    {
        medicationPager.windows = CreateTextLists(patient.medications, medicationTextPrefab, 180);
        medicationPager.InitializePager();
    }

    private void PopulateAllergies(Patient patient)
    {
        allergyPager.windows = CreateTextLists(patient.allergies, allergyTextPrefab, 180);
        allergyPager.InitializePager();
    }

    private List<List<GameObject>> CreateTextLists(List<String> inputList, Text textPrefab, double maxHeight)
    {
        List<List<GameObject>> windows = new List<List<GameObject>>();
        if (inputList.Count == 0)
        {
            return windows;
        }

        List<GameObject> gameObjects = new List<GameObject>();
        double currentHeight = 0;

        foreach (string input in inputList)
        {
            Text text = Instantiate(textPrefab);
            text.text = bulletPoint + input;
            RectTransform rt = text.rectTransform;
            rt.sizeDelta = new Vector2(rt.rect.width, text.preferredHeight);

            if (currentHeight + rt.rect.height <= maxHeight)
            {
                currentHeight += rt.rect.height;
            }
            else
            {
                windows.Add(gameObjects);
                gameObjects = new List<GameObject>();
                currentHeight = rt.rect.height;
            }
            gameObjects.Add(text.gameObject);
        }

        windows.Add(gameObjects);
        return windows;
    }

    private void PopulateLabReports(Patient patient)
    {
        List<List<GameObject>> labReportWindows = new List<List<GameObject>>();
        List<GameObject> labReportList = new List<GameObject>();
        int counter = 0;

        foreach (LabReportData labReportData in patient.labreports)
        {
            GameObject labReportObject = Instantiate(labReportButtonPrefab);
            LabReportManager labReportManager = labReportObject.GetComponent<LabReportManager>();
            labReports.Add(labReportManager);
            labReportManager.labReportData = labReportData;
            labReportManager.collisionDetector = collisionDetector;
            labReportManager.collidingErrorSound = collidingErrorSound;
            labReportManager.placeWindowSound = placeWindowSound;
            labReportManager.moveWindowSound = moveWindowSound;
            Text labReportButtonText = labReportObject.GetComponentInChildren<Text>();
            labReportButtonText.text = labReportData.filename;

            ++counter;
            if (counter % 5 == 0)
            {
                labReportWindows.Add(labReportList);
                labReportList = new List<GameObject>();
            }
            labReportList.Add(labReportObject);
        }

        if (labReportList.Count != 0)
        {
            labReportWindows.Add(labReportList);
        }

        labreportPager.windows = labReportWindows;
        labreportPager.InitializePager();
    }

    private void PopulateVitals(Patient patient)
    {
        PopulateVitals(patient.recentVitals, recentVitalDateText, recentBloodPressure, recentHeartRate,
            recentRespitoryRate, recentPulseOximetry, (int) patient.age);
        PopulateVitals(patient.initialVitals, initialVitalDateText, initialBloodPressure, initialHeartRate,
            initialRespitoryRate, initialPulseOximetry, (int) patient.age);
    }

    private void PopulateVitals(Vitals vitals, Text vitalDateText, Text bloodPressure, 
        Text heartRate, Text respitoryRate, Text pulseOximetry, int patientAge)
    {
        vitalDateText.text = vitals.date.ToString();

        bloodPressure.text = " " + bulletPoint + vitals.systolic + "/" + vitals.diastolic + " mmHg";
        bloodPressure.color = VitalColoring.BloodPressure((int)vitals.systolic, (int)vitals.diastolic);

        heartRate.text = " " + bulletPoint + vitals.heart_rate + " bpm";
        heartRate.color = VitalColoring.HeartRate(patientAge, (int) vitals.heart_rate);

        respitoryRate.text = " " + bulletPoint + vitals.resp_rate + " brpm";
        respitoryRate.color = VitalColoring.RespiratoryRate(patientAge, (int) vitals.resp_rate);

        pulseOximetry.text = " " + bulletPoint + vitals.pulse_oximetry + " sp02";
    }
}
