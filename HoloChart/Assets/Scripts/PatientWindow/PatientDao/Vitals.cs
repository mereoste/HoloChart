﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Vitals : MonoBehaviour {

    public DateTime date;

    //Blood pressure
    public uint systolic;
    public uint diastolic;

    public uint heart_rate;

    public uint resp_rate;

    //Pulse oximetry
    public uint pulse_oximetry;
    
    public Vitals(DateTime date_in, uint bp1, uint bp2, uint heart, uint resp, uint po)
    {
        date = date_in;
        systolic = bp1;
        diastolic = bp2;
        heart_rate = heart;
        resp_rate = resp;
        pulse_oximetry = po;
    }

    public Vitals()
    {
        date = new DateTime(1900, 1, 1);
        systolic = 0;
        diastolic = 0;
        heart_rate = 0;
        resp_rate = 0;
        pulse_oximetry = 0;
    }

    public int isBefore(Vitals v)
    {
        return this.date.CompareTo(v.date);
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
