﻿using System;
using UnityEngine;

public class LabReportData {
    public DateTime dateTaken;
    public string description;
    public string img_filename;
    public string filename;
    public string mrn;

    public LabReportData(string fname_in, string mrn_in)
    {
        if (fname_in == null)
        {
            throw new Exception("filename is null");
        }
        string file_path = "PatientFiles/" + mrn_in + "/" + fname_in;
        TextAsset lab_file = Resources.Load(file_path) as TextAsset;
        if (!lab_file)
        {
            throw new Exception("Invalid lab file - MRN is " + mrn_in + ", fname_in is " + fname_in +
                ", full path is " + file_path);
        }
        string[] arr = lab_file.text.Split("\n"[0]);

        description = arr[0].TrimEnd('\r');
        char[] comma_delims = { ',' };

        string[] date_str = arr[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
        if (date_str.Length != 3)
        {
            throw new System.Exception("length of date_str is " + date_str.Length +
                ", line read in is " + arr[1]);
        }
        string year = date_str[0];
        string month = date_str[1];
        string day = date_str[2];
        dateTaken = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));

        filename = fname_in;
        mrn = mrn_in;
        img_filename = arr[2].TrimEnd('\r');
    }
}






