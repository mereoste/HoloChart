using System;
using UnityEngine;
using System.Collections.Generic;

//Patient Database (DAO) Object
public class PatientDao : MonoBehaviour
{
    public Patient patient;

    private string pathname = "PatientFiles/";
    private string infofile = "patient_info";

    //Single-field attributes
    public const string NAME = "Name";
    public const string MED_RECORD_NUM = "Medical Record Number";
    public const string AGE = "Age";
    public const string SEX = "Sex";
    public const string RESPIRATORY_RATE = "Respiratory Rate";
    public const string HEART_RATE = "Heart Rate";

    //NB units stored as metric, converted to lbs upon request
    public const string WEIGHT = "Weight";
    public const string POUNDS = "LBS";
    public const string KG = "KG";

    public const string HEIGHT = "Height";
    public const string CENTIMETERS = "cm";
    public const string IMPERIAL_HEIGHT = "Imperial";

    //Values are comma-seperated
    public const string ALLERGIES = "Allergies";
    public const string MEDICATIONS = "Medications";
    public const string BLOOD_PRESSURE = "Blood Pressure";
    public const string PULSE_OXIMETRY = "Pulse Oximetry";
    public const string LAB_REPORTS = "Lab Reports";
    public const string BIRTHDAY = "Birthday";
    public const string SYMPTOMS = "Symptoms";
    public const string VITALS_DATES = "Vitals Dates";

    public Patient GetPatient(string patientIdentifier)
    {
        TextAsset patientFile = Resources.Load(pathname + patientIdentifier + '/' + infofile) as TextAsset;
        if (!patientFile)
        {
            throw new Exception("Patient " + patientIdentifier + " not found");
        }

        string name = "";
        string med_record_num = "";
        uint age = 15;
        string sex = "";
        double weight_pounds = 0;
        List<String> allergies = new List<String>();
        uint height_inches = 0;

        List<DateTime> vitals_dates_list = new List<DateTime>();
        List<uint> heart_rate_list = new List<uint>();
        List<Tuple<uint, uint>> blood_pressure_pair_list = new List<Tuple<uint, uint>>();
        List<uint> pulxe_ox_list = new List<uint>();
        List<uint> resp_rate_list = new List<uint>();

        DateTime birthday = new DateTime(1996, 11, 22);
        List<String> symptoms = new List<String>();
        List<String> medications = new List<String>();
        List<String> labreport_filenames = new List<String>();

        foreach (String line in patientFile.text.Split("\n"[0]))
        {
            //Check for a comment or blank line
            if ((line.Length == 0) || (line[0] == '#')) { continue; }

            //Split by colons 
            char[] colon = { ':' };
            string[] strPair = line.Split(
                colon, StringSplitOptions.RemoveEmptyEntries);

            //Declare delims
            char[] comma_delims = { ',' };
            char[] semicolon_delims = { ';' };

            switch (strPair[0])
            {

                case VITALS_DATES:
                    {
                        string[] vals_str = strPair[1].Split(semicolon_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string entry in vals_str)
                        {
                            //Each should have 3
                            string[] date_data = entry.Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                            vitals_dates_list.Add(new DateTime(Convert.ToInt32(date_data[0]),
                                Convert.ToInt32(date_data[1]), Convert.ToInt32(date_data[2])));
                        }
                        break;
                    }

                case NAME:
                    {
                        name = strPair[1].TrimEnd('\r');
                        break;
                    }

                case MED_RECORD_NUM:
                    {
                        med_record_num = strPair[1].TrimEnd('\r');
                        break;
                    }

                case AGE:
                    {
                        age = Convert.ToUInt32(strPair[1]);
                        break;
                    }

                case SEX:
                    {
                        sex = strPair[1].TrimEnd('\r'); ;
                        break;
                    }

                case RESPIRATORY_RATE:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in vals_str)
                        {
                            resp_rate_list.Add(Convert.ToUInt32(s));
                        }
                        break;
                    }

                case HEART_RATE:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in vals_str)
                        {
                            heart_rate_list.Add(Convert.ToUInt32(s));
                        }
                        break;
                    }

                case WEIGHT:
                    {
                        weight_pounds = Convert.ToDouble(strPair[1]);
                        break;
                    }

                case HEIGHT:
                    {
                        height_inches = Convert.ToUInt32(strPair[1]);
                        break;
                    }

                case ALLERGIES:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in vals_str)
                        {
                            allergies.Add(s);
                        }
                        break;
                    }

                case MEDICATIONS:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in vals_str)
                        {
                            medications.Add(s);
                        }
                        break;
                    }

                case LAB_REPORTS: {
                    string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string s in vals_str)
                    {
                            labreport_filenames.Add(s.TrimEnd('\r'));
                    }
                    break;
                }

                case BLOOD_PRESSURE:
                    {
                        string[] vals_str = strPair[1].Split(semicolon_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string entry in vals_str)
                        {
                            string[] entry_str = entry.Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                            uint bp1 = Convert.ToUInt32(entry_str[0]);
                            uint bp2 = Convert.ToUInt32(entry_str[1]);
                            Tuple<uint, uint> t = new Tuple<uint, uint>(bp1, bp2);
                            blood_pressure_pair_list.Add(t);
                        }
                        break;
                    }

                case PULSE_OXIMETRY:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string entry in vals_str)
                        {
                            pulxe_ox_list.Add(Convert.ToUInt32(entry));
                        }
                        break;
                    }
                case BIRTHDAY:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        //Order: year,month,day
                        birthday = new DateTime(Convert.ToInt32(vals_str[0]), Convert.ToInt32(vals_str[1]), Convert.ToInt32(vals_str[2]));
                        break;
                    }
                case SYMPTOMS:
                    {
                        string[] vals_str = strPair[1].Split(comma_delims, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in vals_str)
                        {
                            symptoms.Add(s.TrimEnd('\r'));
                        }
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        //Create and send list of vitals
        List<Vitals> v_list = new List<Vitals>();
        for (int i = 0; i < vitals_dates_list.Count; i++)
        {
            Vitals v = new Vitals(vitals_dates_list[i], blood_pressure_pair_list[i].Item1,
                blood_pressure_pair_list[i].Item2, heart_rate_list[i],
                resp_rate_list[i], pulxe_ox_list[i]);
            v_list.Add(v);
        }

        List<LabReportData> labreports = new List<LabReportData>();
        foreach (String labreport_filename in labreport_filenames)
        {
            labreports.Add(new LabReportData(labreport_filename, med_record_num));
        }

        return new Patient(name, med_record_num, age, sex, weight_pounds, allergies,
            height_inches, birthday, symptoms, labreports, medications, v_list);
    }//end of method
}//end of class
