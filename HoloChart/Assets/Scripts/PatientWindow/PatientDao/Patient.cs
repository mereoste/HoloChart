using System;
using System.Collections.Generic;

public class Patient
{
    public string name, mrn, sex;

    public double weight_pounds;
    public uint height_inches, age;

    public DateTime birthday;

    public List<string> symptoms, medications, allergies;
    public List<LabReportData> labreports;

    public Vitals initialVitals, recentVitals;
    public List<Vitals> v_list;

    public Patient(string name_in, string mrn_in, uint age_in, string sex_in,
        double weight_in, List<string> allergies_in, uint height_in, DateTime birthday_in,
        List<string> symptoms_in, List<LabReportData> labreports_in,
        List<string> meds_in, List<Vitals> v_in)
    {
        v_list = v_in;
        //Sort by date - ONLY DONE HERE
        v_list.Sort((x, y) => -(x.isBefore(y)));
        initialVitals = v_list[0];
        recentVitals = v_list[1];

        symptoms = symptoms_in;
        medications = meds_in;
        allergies = allergies_in;
        labreports = labreports_in;

        name = name_in;
        mrn = mrn_in;
        age = age_in;
        sex = sex_in;
        weight_pounds = weight_in;

        height_inches = height_in;
        birthday = birthday_in;
    }

    //Returns all the descriptions of the lab reports
    public List<string> labReportDescriptions()
    {
        List<string> descs = new List<string>();
        foreach(LabReportData lr in labreports)
        {
            descs.Add(lr.description);
        }
        return descs;
    }
}
