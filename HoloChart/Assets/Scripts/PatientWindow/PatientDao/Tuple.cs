﻿using System;

//Sub for Tuple 
// C. of Dean Chalk, StackOverflow user
public class Tuple<T, U>
{

    public T Item1 { get; private set; }
    public U Item2 { get; private set; }


    public Tuple(T t, U u)
    {
        Item1 = t;
        Item2 = u;
    }


}
