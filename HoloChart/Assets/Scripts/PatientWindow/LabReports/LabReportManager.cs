using System;
using UnityEngine;

public class LabReportManager : WindowManager
{
    public LabReportData labReportData;

    public override GameObject CreateWindow()
    {
        GameObject window = Instantiate(windowPrefab);
        LabReportWindow labReportWindow = window.GetComponent<LabReportWindow>();
        labReportWindow.labImage.sprite = LoadSprite();
        labReportWindow.title.text = labReportData.filename;
        labReportWindow.date.text = labReportData.dateTaken.ToString();
        labReportWindow.description.text = labReportData.description;
        return window;
    }

    private Sprite LoadSprite()
    {
        string spriteFilename = "PatientFiles/" + labReportData.mrn + "/" + labReportData.img_filename;
        Sprite imageSprite = (Sprite)Resources.Load(spriteFilename, typeof(Sprite));

        if (!imageSprite)
        {
            throw new Exception("Invalid image filepath: " + spriteFilename);
        }

        return imageSprite;
    }
}