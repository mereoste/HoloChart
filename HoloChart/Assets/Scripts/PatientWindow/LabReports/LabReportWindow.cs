﻿using UnityEngine.UI;

public class LabReportWindow : WindowObject {
    public Image labImage;
    public Text title;
    public Text date;
    public Text description;
}
