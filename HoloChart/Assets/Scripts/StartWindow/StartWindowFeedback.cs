﻿using UnityEngine;
using UnityEngine.UI;

public class StartWindowFeedback : MonoBehaviour
{
    public Text mrnInputText;
    public Text feedbackText;

    private string defaultFeedback = "Please enter patient's medical record number";

    public void Update()
    {
        if (mrnInputText.text.Length == 0)
        {
            feedbackText.color = Color.white;
            feedbackText.text = defaultFeedback;
        }
    }

    public void DisplayErrorMessage(string message)
    {
        feedbackText.color = Color.red;
        feedbackText.text = message;
    }
}
