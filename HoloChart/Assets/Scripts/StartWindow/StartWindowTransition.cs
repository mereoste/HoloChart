﻿using System;
using System.Net;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartWindowTransition : MonoBehaviour
{
    public PatientDao patientDao;
    public DiagnosticWindowStateManager diagnosticWindowManager;

    public Text mrnInputText;
    public Text feedbackText;
    public StartWindowFeedback displayFeedback;

    public InputField mrnInputField;

    public GameObject patientWindow;
    public GameObject diagnosticWindow;
    public GameObject controlWindow;
    //Init in Unity
    public CollisionDetector de;

    public PatientWindowManager patientManager;

    public AudioSource loginSound;

    public void Transition()
    {
        string userInputtedMRN = mrnInputText.text;

        try
        {
            if (!patientManager.mrn.Equals(userInputtedMRN))
            {
                Patient patient = patientDao.GetPatient(userInputtedMRN);
                //Get initially reported symptoms from patient
                List<string> patient_symptoms = new List<string>(patient.symptoms);
                //Add possible symptoms from lab report
                patient_symptoms.AddRange(patient.labReportDescriptions());
                diagnosticWindowManager.InitPatientSymptoms(patient_symptoms, (int)patient.age, patient.sex);
                patientManager.PopulatePatientWindow(patient);
            }

            //No effect
            mrnInputText.text = "";

            mrnInputField.Select();
            mrnInputField.text = "";

            loginSound.Play();

            patientWindow.SetActive(true);
            diagnosticWindow.SetActive(true);
            controlWindow.SetActive(true);
            gameObject.SetActive(false);
            

            if (de != null)
            {
                de.addToDetector(patientWindow.GetComponent<RectTransform>(), false);
                de.addToDetector(diagnosticWindow.GetComponent<RectTransform>(), false);
                de.addToDetector(controlWindow.GetComponent<RectTransform>(), false);
            }
        }
        catch (WebException we)
        {
            Debug.Log(we);
            displayFeedback.DisplayErrorMessage("Internet connection not working");
        }
        catch (Exception e)
        {
            Debug.Log(e);
            displayFeedback.DisplayErrorMessage("Patient MRN: \"" + userInputtedMRN + "\" not found");
        }
    }
}
